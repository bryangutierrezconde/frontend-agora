import { Grid } from "@mui/material";
import NavBarPage from "@/components/navbar";
import BodyPage from "@/components/body";
import InfoPage from "@/components/info";
import CardImage from "@/components/cardImage";

export default function Home() {
  const ImageCard = [
    {
      image: "/images/Mujer-emprendedora.png",
      title: "Emprendimientos y pequeñas empresas en crecimiento",
      description:
        "Ofrecemos formación empresarial, vinculación a mercados y acceso a financiamiento a emprendedores y pequeñas empresas en crecimiento en su camino hacia la sostenibilidad económica, social y ambiental.",
    },
    {
      image: "/images/planeacion.png",
      title: "Ecosistema",
      description:
        "Contribuimos al fortalecimiento del ecosistema de emprendedores y pequeñas empresas en crecimiento a través de eventos, tales como el programa Formando Catalizadores, y el co-liderazgo de la Red de Impacto, integrada por más de 100 organizaciones de la región.",
    },
    {
      image: "images/empresario-levantando-la-mano.png",
      title: "Proyectos inclusivos",
      description:
        "Ejecutamos proyectos específicos junto a aliados con los que compartimos nuestra visión por la inclusión financiera, la equidad de género, la diversificación, resiliencia y sostenibilidad en las cadenas de valor, y la adaptación al cambio climático en la región.",
    },
  ];
  return (
    <Grid
      container
      spacing={2}
      style={{
        backgroundColor: "whitesmoke",
      }}
    >
      <Grid item xs={12}>
        <NavBarPage />
      </Grid>
      <Grid item xs={12}>
        <BodyPage />
      </Grid>
      <Grid item xs={12}>
        <InfoPage
          info={"¿Que hacemos?"}
          description={
            "Calibramos el ecosistema emprendedor de América Latina para crear prosperidad inclusiva para el individuo, la empresa, la comunidad y el medio ambiente."
          }
          linkName={"Conocer mas sobre nuestros programas"}
          link={"/"}
        />
      </Grid>
      <CardImage data={ImageCard} />
    </Grid>
  );
}
