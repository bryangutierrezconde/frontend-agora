import { Card, Grid, Typography } from "@mui/material";
import React from "react";

const CardImage: React.FC<{
  data: any;
}> = ({ data }) => {
  return (
    <Grid
      container
      spacing={2}
      style={{
        paddingLeft: "10%",
        paddingRight: "10%",
        paddingTop: "5%",
        boxSizing: "border-box",
      }}
    >
      {data.map((card: any, index: number) => (
        <Grid item xs={4} key={index}>
          <Card style={{ backgroundColor: "whitesmoke" }}>
            <Grid container spacing={2}>
              <Grid
                className="cardImage"
                item
                xs={12}
                style={{ width: "100%", marginBottom: "5%" }}
              >
                <img
                  style={{ borderRadius: 15 }}
                  src={card.image}
                  alt="image"
                />
              </Grid>
              <Grid
                className="cardColor"
                item
                xs={12}
                style={{
                  marginLeft: "5%",
                  padding: "10%",
                  width: "100%",
                }}
              >
                <Typography
                  style={{ marginBottom: 15 }}
                  fontSize={18}
                  fontWeight={"bold"}
                >
                  {card.title}
                </Typography>
                <Typography fontSize={14}>{card.description}</Typography>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};
export default CardImage;
