import Link from "next/link";
import React from "react";

const BodyPage = () => {
  return (
    <Link href={"/"}>
      <img
        src="https://agora2030.org/wp-content/uploads/2024/01/Banner-sitio-web-1-jpg.webp"
        style={{ width: "100%", marginTop: "4%" }}
      />
    </Link>
  );
};
export default BodyPage;
