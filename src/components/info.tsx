import { Grid, Typography } from "@mui/material";
import Link from "next/link";
import React from "react";

const InfoPage: React.FC<{
  info: string;
  description: string;
  linkName: string;
  link: string;
}> = ({ info, description, linkName, link }) => {
  return (
    <Grid
      container
      spacing={2}
      style={{ paddingRight: "10%", paddingLeft: "10%", paddingTop: "10%" }}
    >
      <Grid item xs={6}>
        <Typography
          variant="h4"
          color={"#0d75b0"}
          fontWeight={"bold"}
          style={{ marginBottom: 10 }}
        >
          {info}
        </Typography>
        <Typography color={"gray"} style={{ width: "80%" }}>
          {description}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Link href={link}>
          <Typography textAlign={"right"} color={"#0d75b0"} fontWeight={"bold"}>
            {linkName}
          </Typography>
        </Link>
      </Grid>
    </Grid>
  );
};
export default InfoPage;
