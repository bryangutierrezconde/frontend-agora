import {
  AppBar,
  Button,
  Grid,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@mui/material";
import Link from "next/link";
import Image from "next/image";
import React, { useState } from "react";
const NavBarPage = () => {
  const pagesNames = [
    "Quienes somos",
    "Programas",
    "Impacto",
    "Sumate",
    "Blog",
    "Recursos",
    "Sedes",
  ];
  const [es, setEs] = useState<boolean>(true);
  const SetLanguage = () => {
    if (es) {
      setEs(false);
    } else {
      setEs(true);
    }
  };
  return (
    <AppBar
      position="fixed"
      style={{
        backgroundColor: "whitesmoke",
        height: 75,
      }}
    >
      <Grid
        container
        spacing={2}
        style={{ width: "100%", height: "100%", margin: 0, padding: 0 }}
      >
        <Grid
          item
          xs={2.5}
          style={{
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Link href={"/"}>
            <Image
              src={"/images/logo-14.png"}
              alt="logo-14"
              width={110}
              height={110}
            />
          </Link>
        </Grid>
        <Grid
          item
          xs={6}
          style={{
            width: "100%",
            height: "100%",
          }}
        >
          <Toolbar style={{ margin: 0, padding: 0 }}>
            {pagesNames.map((name) => (
              <Link href={"/"} key={name}>
                <Typography
                  className="simpleColor"
                  color={"gray"}
                  fontFamily={"sans-serif"}
                  fontStyle={"unset"}
                  fontSize={14}
                  style={{ marginBottom: 15, marginLeft: 15 }}
                >
                  {name}
                </Typography>
              </Link>
            ))}
            <Link href={"/"}>
              <Typography
                className="redColor"
                fontFamily={"sans-serif"}
                fontSize={14}
                fontWeight={"bold"}
                style={{ marginBottom: 15, marginLeft: 30 }}
              >
                Change Lab
              </Typography>
            </Link>
          </Toolbar>
        </Grid>
        <Grid
          item
          xs={3.5}
          style={{
            width: "100%",
            height: "100%",
            margin: 0,
            padding: 0,
            display: "flex",
            alignItems: "center",
          }}
        >
          <Button
            style={{
              backgroundColor: "#cc2c64",
              paddingLeft: 25,
              paddingRight: 25,
              paddingTop: 0,
              paddingBottom: 0,
              borderRadius: 15,
              marginRight: 15,
              textTransform: "none",
            }}
          >
            <Typography color={"white"} fontFamily={"sans-serif"} fontSize={14}>
              Donar
            </Typography>
          </Button>
          <Button
            onClick={SetLanguage}
            style={{
              backgroundColor: es ? "gray" : "white",
              paddingLeft: 0,
              paddingRight: 0,
              marginLeft: 0,
              marginRight: 0,
            }}
          >
            <Typography
              style={{
                margin: 0,
                padding: 0,
                color: es ? "whitesmoke" : "gray",
              }}
              fontWeight={es ? "bold" : ""}
              fontSize={12}
            >
              es
            </Typography>
          </Button>
          <Button
            onClick={SetLanguage}
            style={{
              backgroundColor: !es ? "gray" : "white",
              paddingLeft: 0,
              paddingRight: 0,
              marginLeft: 0,
              marginRight: 0,
            }}
          >
            <Typography
              style={{
                margin: 0,
                padding: 0,
                color: !es ? "whitesmoke" : "gray",
              }}
              fontWeight={!es ? "bold" : ""}
              fontSize={12}
            >
              en
            </Typography>
          </Button>
        </Grid>
      </Grid>
    </AppBar>
  );
};

export default NavBarPage;
